import React, { Component } from 'react';
import { 
  View, 
  Text,
  Dimensions,
  ToastAndroid,
  AlertIOS,
  ScrollView,
  Platform,
  StatusBar,
  Button,
  ImageBackground
} from 'react-native';

import { Header } from '../common';
import TextField from 'react-native-text-field';
const HomeImages = [require('../assests/images/Home.jpg'),require('../assests/images/Home2.jpg'),require('../assests/images/Home3.jpg'),require('../assests/images/Home4.jpg')];
let imageIndex = Math.floor(Math.random()*(HomeImages.length));
const Finance = require('financejs');
const finance = new Finance();
const { format } = require('number-currency-format');
import { TextInputMask } from 'react-native-masked-text'
export default class Converter extends Component {
 constructor(props) {
  super(props);
    this.state = {
      rate:12,
      amount:null,
      error: false,
      year:null,
      totalIncome:null,
      result:"0.00",
      suggesstionVisible:false
    };
    this.npv = this.npv.bind(this);
    this.totalAnnualIncome = this.totalAnnualIncome.bind(this);
  }

  componentDidMount() {
    const t = this;
    t.setState({rate:15,suggesstionVisible:false});
  }

  npv(){
    const t = this;
    let cashFlows = [];
    let getIncome = t.totalAnnualIncome();
    
    for (let i = 0; i < t.state.year-1 ; i++) {
          cashFlows.push(parseInt(getIncome));
    }
    cashFlows.push(parseInt(getIncome)+parseInt(t.state.totalIncome));
    console.warn(cashFlows);
    let result = finance.NPV(t.state.rate, 0,...cashFlows);
    if(!isNaN(result)){
      t.setState({result:format(result),suggesstionVisible:true});
      setTimeout(function(){
          t.setState({suggesstionVisible:false});
      },30000);
    }
    else{
      const msg="Lütfen bütün alanları doldurunuz!"
      t.setState({result:"0.00",suggesstionVisible:false});
      if (Platform.OS === 'android') {
        ToastAndroid.show(msg, ToastAndroid.LONG)
      } else {
        AlertIOS.alert(msg);
      }
    }
    
  }

  totalAnnualIncome(){
      const t = this;
      const income = parseInt(t.state.amount)*12;
      if(income<22000){
        return parseInt(income) - ((parseInt(income)*15)/100);
      }
      else if(income<50000){
          return  parseInt(income) - (((parseInt(income)-22000)*20/100)+3300);
      }
      else if(income<121000){
        return  parseInt(income) -(((parseInt(income)-49000)*27/100)+8700);
      }
      else if(income<601000){
        return  parseInt(income) -(((parseInt(income)-120000)*35/100)+27870);
      }
      else{
        return  parseInt(income) -(((parseInt(income)-600000)*40/100)+195870);
      }
  }
  numberContol(val){
    
  }
  formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
  
      const negativeSign = amount < 0 ? "-" : "";
  
      let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
      let j = (i.length > 3) ? i.length % 3 : 0;
  
      return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
      console.log(e);
      return 1;
    }
  }

  render() {
    return (
      <View style={styles.viewStyle}>
        <StatusBar
          backgroundColor="#1d3557"
          barstyle="light-content"
        />
        <Header headerText='Gayrimenkul Gerçek Değeri'/>
          <ScrollView contentContainerStyle={styles.baseView}>
             
             <View style={styles.resultContainer}>
             <Text
              style={{fontSize:50,position:'absolute',bottom:0,right:0,color:"#000",opacity:1}}
             >
               {this.state.result}
             </Text>
             
                <ImageBackground source={HomeImages[imageIndex]} style={styles.image}>
                   
                  </ImageBackground>
                 
              </View>
              <View>
                <Text style={{color:'#e63946',textAlign:'right'}}>Ortalama faiz %12 olarak hesaplanmıştır. </Text>
              </View>
              <View style={styles.convertInputs}>
              <TextInputMask
                style={styles.inputStyles}
                type={'money'}
                value={this.state.amount}
                onChangeText={(amount) => this.setState({amount})}
                options={{
                  precision: 0,
                  separator: ',',
                  delimiter: '.',
                  unit: '',
                  suffixUnit: ''
                }}
                
                placeholder={'Ortalama Kira Bedeli'}
                />      
              </View>
            
             <View style={styles.convertInputs}>
               
             <TextInputMask
                placeholder="Kaç Yıl Sonra Satmayı Düşünürsünüz"
                onChangeText={(year) => this.setState({year})}
                type={'only-numbers'} 
                value={this.state.year}
                style={styles.inputStyles}
                />
             </View>
             <View style={styles.convertInputs}>
             <TextInputMask
                placeholder={this.state.year ? this.state.year +" Yıl Sonra Satmak İstediğiniz Fiyat":"Satmak İstediğiniz Fiyat"}
                type={'money'}
                onChangeText={(totalIncome) => this.setState({totalIncome})}
                value={this.state.totalIncome}
                style={styles.inputStyles}
                options={{
                  precision: 0,
                  separator: ',',
                  delimiter: '.',
                  unit: '',
                  suffixUnit: ''
                }}
                />
             </View>
             <View >

                          <Button
                          title="Hesapla"
                          color="#e63946"
                          onPress={this.npv.bind()}
                          />
                     
             </View>
             { this.state.suggesstionVisible ?
                  <View style={{backgroundColor:'#a8dadc',marginTop:10,padding:8,width:Dimensions.get('window').width-30}}>
                  <Text style={{color:'#1d3557',fontSize:16}}>
                      Girmiş oluduğunuz değerlere göre gerçek değer hesaplama fonksiyonu (NPV) gayrimenkul fiyatını  {this.state.result}₺ olarak hesaplamıştır. Kira vergisi,enflasyon oranı,faiz yüzdesi bugünün değerleri baz alınarak hesaplanmıştır.
                   </Text>
                  </View>
                  : null
             }
            
            <View style={{ flex: 3 }}></View>
          </ScrollView>
      </View>
    );
  }
}
const styles = {
 
  viewStyle: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  baseView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  image: {
    marginTop:10,
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    opacity:0.5
  },
  textView: {
    fontSize: 25,
    color: '#666666',
    paddingRight: 10
  },
  inputStyles: {
    width: Dimensions.get('window').width-30,
    height: Dimensions.get('window').height/16,
    borderWidth: 1, 
    borderColor: '#eee' ,
    color: '#000',
    fontSize: 18,
    margin:2,
    paddingBottom: 0
  },
  InputRowContainer:
  {
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'stretch',
                      marginTop:10
},
  convertInputs: {
    flexDirection: 'row',
    marginTop:5
  },
  resultContainer:{
    height:Dimensions.get('window').height/4
  },
  totalAmountView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: Dimensions.get('window').width/2.0,
    height: Dimensions.get('window').height/16,
    borderRadius: Dimensions.get('window').width/2.1,
    margin: 20,
    marginTop: 30,
    backgroundColor: '#61b207'
  },
  totalAmountText: {
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 17,
    fontFamily: 'Roboto-Bold',
  },
  dateView: {
    width: Dimensions.get('window').width/2.1,
    height: Dimensions.get('window').height/20,
    flexDirection: 'row', 
    justifyContent: 'space-around', 
  },
  dateText : {
    fontSize: 18, 
    color: '#000'
  },
  textStyles: {
    fontSize: 13, 
    color: '#000', 
    fontFamily: 'Roboto-Medium'
  },
}
