import React, { Component } from 'react'
import { 
  View, 
  Text,
  Linking,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Zocial';
import { Header, Card } from '../common';

export default class Help extends Component {
   constructor(props) {
    super(props);
    this.state = {
    };
  }

  openURL(url) {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Not supported");
      }
    });
  }

  render() {
    return (
      <ScrollView style={styles.baseView}>
        <Header headerText='Yardım' />
        <View style={styles.baseView}>
          <View style={{ flex: 0.6, marginTop: 20 }}>
            
            <View style={{ alignItems: 'center'}}>
              <Text style={[styles.text, { fontWeight: 'bold', fontSize: 18 }]}>Gayrimenkul Gerçek Değer Hesaplama</Text>
            </View>
            <View style={{ justifyContent: 'center', padding: 10 }}>
              <Text style={[styles.text, { lineHeight: 25 }]}>
              Uygulamamız bir gayrimenkulun bulunduğu zaman dilimindeki gerçek değerini hesaplar. Gayrimenkul satışlarında, üst limit denetimi yapılmadığı için fiyatlar orantısız ve mantığa dayalı olmadan artmaktadır. Gayrimenkul almaya niyetlenen vatandaşlarımız için güvenilir bir uygulamadır. Hesaplamalar, bulunduğu zaman diliminde ki ortamın şartlarına bağlı olarak, gayrimenkulun kira değeri, enflasyon oranı, faiz yüzdesi işleme sokularak yapılır. Hesaplama yapılırken, finans sektöründe önemli bir yeri olan NPV (Bugünkü Net Değer) fonksiyonundan yararlanılmıştır. Fonksiyon ile ilgili linkler aşağıda paylaşılmıştır.              </Text>
            </View>
            <View style={{ alignItems: 'center'}}>
              <Text style={[styles.text, { fontWeight: 'bold', fontSize: 18 }]}>Örnek</Text>
            </View>
            <View style={{ justifyContent: 'center', padding: 10 }}>
              <Text style={[styles.text, { lineHeight: 25 }]}>
                Uygulamanın çalışma mantığını basit bir şekilde anlatalım. Antalya/Konyaaltın'da bir ev almak istiyorum. Fakat söylenen değerin, evin gerçek değeri olduğunu anlamak için NPV hesaplaması yapmak gerekir.
              </Text>
              <Text style={[styles.text, { fontWeight: 'bold', fontSize: 16 }]}>
              {'\n'} Uygulamadaki ilk değer: <Text style={[styles.text],{fontWeight:'normal'}}>
                Evin bulunduğu konumda ki ortalama kira değeri.
              </Text>
              </Text>
              <Text style={[styles.text, { fontWeight: 'bold', fontSize: 16 }]}>
              {'\n'} Uygulamadaki ikinci değer: <Text style={[styles.text],{fontWeight:'normal'}}>
                Yıl içerisinde ki faiz yüzdesi.
              </Text>

              </Text>
              <Text style={[styles.text, { fontWeight: 'bold', fontSize: 16 }]}>
              {'\n'} Uygulamadaki üçüncü değer: <Text style={[styles.text],{fontWeight:'normal'}}>
                Evi satmayı düşünürseniz kaç yıl içerisin de satarsanız. Eğer kredi çekerek ev alacaksanız, kredi vade olarak da düşünebilirsiniz. 
              </Text>
                
              </Text>

              <Text style={[styles.text, { fontWeight: 'bold', fontSize: 16 }]}>
              {'\n'} Uygulamadaki dördünce değer: <Text style={[styles.text],{fontWeight:'normal'}}>
                Evi satmak istediğinizde ortalama değerini girebilirsiniz. 
              </Text>
                
              </Text>
              <Text style={[styles.text]}>
                {'\n'}
                  Yukarıdaki değerler alındıktan sonra mevcut zaman dilimindeki kira vergisi oranı,faiz yüzdesi,enflasyon oranı baz alınarak yıllık net kira kazancı elde edilir. Bu 
                  değerlerle NPV fonksiyonunda  gerekli hesaplamalar yapıldıktan sonra gerçek değer elde edilecektir. 
              </Text>
                
              
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', padding: 15 }}>
            <Text style={[styles.text, { fontWeight: 'bold', fontSize: 18 }]}>Faydalı Linkler</Text>
              <Text style={[styles.text, { lineHeight: 25 }]}>
                <Text 
                  style={[styles.text, { color: '#2b51ef', marginLeft: 2, marginRight: 2 }]}
                  onPress={() => this.openURL('https://tr.wikipedia.org/wiki/Net_bug%C3%BCnk%C3%BC_de%C4%9Fer')}
                >
                  {' '} Paranın Bugünkü Değeri - Wiki(TR) {' '} 
                </Text>
              </Text>
              <Text style={[styles.text, { lineHeight: 25 }]}>
                <Text 
                  style={[styles.text, { color: '#2b51ef', marginLeft: 2, marginRight: 2 }]}
                  onPress={() => this.openURL('https://en.wikipedia.org/wiki/Net_present_value')}
                >
                  {' '} Net Present Value - Wiki(EN) {' '} 
                </Text>
              </Text>
              <Text style={[styles.text, { lineHeight: 25 }]}>
                <Text 
                  style={[styles.text, { color: '#2b51ef', marginLeft: 2, marginRight: 2 }]}
                  onPress={() => this.openURL('http://www.gokii.net/2015/10/paranin-bugunku-degeri-npv/')}
                >
                  {' '} Paranın Bugünkü Değeri (NPV) {' '} 
                </Text>
              </Text>
            </View>
            
          </View>
          
        </View>
      </ScrollView>
    )
  }
}

const styles = {
  baseView: {
    flex: 1, 
    backgroundColor: '#fff',
  },
  text: {
    color: '#000',
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    textAlign: 'center'
  }
}
