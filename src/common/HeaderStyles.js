export default {
  viewStyle: {
    backgroundColor: '#1d3557',
    height: 35,
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2,
    position: 'relative',
    flexDirection: 'row',
    paddingLeft: 10,
  },
  menuViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 2
  },
  headerViewStyle: {
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
    flex: 12,
    flexDirection: 'row',
    alignItems: 'center'
  },
  headerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 18,
    color: '#fff',
    marginBottom:5
  }
}