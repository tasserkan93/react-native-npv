# Gayrimenkul Gerçek Değer Hesaplama

Uygulamamız bir gayrimenkulun bulunduğu zaman dilimindeki gerçek değerini hesaplar. Gayrimenkul satışlarında, üst limit denetimi yapılmadığı için fiyatlar orantısız ve mantığa dayalı olmadan artmaktadır. Gayrimenkul almaya niyetlenen vatandaşlarımız için güvenilir bir uygulamadır. Hesaplamalar, bulunduğu zaman diliminde ki ortamın şartlarına bağlı olarak, gayrimenkulun kira değeri, enflasyon oranı, faiz yüzdesi işleme sokularak yapılır. Hesaplama yapılırken, finans sektöründe önemli bir yeri olan NPV (Bugünkü Net Değer) fonksiyonundan yararlanılmıştır. Fonksiyon ile ilgili linkler aşağıda paylaşılmıştır.

## Örnek
Uygulamanın çalışma mantığını basit bir şekilde anlatalım. Antalya/Konyaaltın'da bir ev almak istiyorum. Fakat söylenen değerin, evin gerçek değeri olduğunu anlamak için NPV hesaplaması yapmak gerekir.

1.  Uygulamadaki ilk değer: Evin bulunduğu konumda ki ortalama kira değeri.
2.  Uygulamadaki ikinci değer:  Yıl içerisinde ki faiz yüzdesi.
3.  Uygulamadaki üçüncü değer: Evi satmayı düşünürseniz kaç yıl içerisin de satarsanız. Eğer kredi çekerek ev alacaksanız, kredi vade olarak da düşünebilirsiniz. 
4.  Uygulamadaki dördünce değer:  Evi satmak istediğinizde ortalama değerini girebilirsiniz. 

Yukarıdaki değerler alındıktan sonra mevcut zaman dilimindeki kira vergisi oranı,faiz yüzdesi,enflasyon oranı baz alınarak yıllık net kira kazancı elde edilir. Bu değerlerle NPV fonksiyonunda  gerekli hesaplamalar yapıldıktan sonra gerçek değer elde edilecektir. 